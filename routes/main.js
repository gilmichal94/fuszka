const express = require('express');
const router = express.Router();

// show index page
router.get('/', (req, res) => {
    res.render('index');
});

router.get('*', (req, res) => {
    res.render('404');
});

module.exports = router;