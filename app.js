const express = require('express'),
app = express(),
bodyParser = require('body-parser');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

// routes
const mainRoutes = require('./routes/main');
app.use(mainRoutes);

// listening server
app.listen(3000, () => {
    console.log('Fuszka is running on port 3000');
})